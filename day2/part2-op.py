#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2019 Tiarnan de Burca <tdeburca@gmail.com>

import sys


def main():
    # get all sets of noun/verb combinations.
    combos = []
    for i in range(0, 100):
        for j in range(0, 100):
            combos.append([i, j])

    cursor = len(combos/2)
    target = 19690720
    noun, verb = combos[cursor]
    result = runProgram(noun, verb)
    if result == target:
        print noun, verb, result
        sys.exit()
    found = False
    while found != True:
        noun, verb = combos[cursor]
        result = runProgram(noun, verb)
        if result == target:
            print noun, verb, result
            sys.exit()
        elif result > target:
            cursor = len(combos) -


def runProgram(noun, verb):
    intcode = [
        1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 10, 1, 19, 1, 19, 9,
        23, 1, 23, 6, 27, 2, 27, 13, 31, 1, 10, 31, 35, 1, 10, 35, 39, 2, 39,
        6, 43, 1, 43, 5, 47, 2, 10, 47, 51, 1, 5, 51, 55, 1, 55, 13, 59, 1, 59,
        9, 63, 2, 9, 63, 67, 1, 6, 67, 71, 1, 71, 13, 75, 1, 75, 10, 79, 1, 5,
        79, 83, 1, 10, 83, 87, 1, 5, 87, 91, 1, 91, 9, 95, 2, 13, 95, 99, 1, 5,
        99, 103, 2, 103, 9, 107, 1, 5, 107, 111, 2, 111, 9, 115, 1, 115, 6,
        119, 2, 13, 119, 123, 1, 123, 5, 127, 1, 127, 9, 131, 1, 131, 10, 135,
        1, 13, 135, 139, 2, 9, 139, 143, 1, 5, 143, 147, 1, 13, 147, 151, 1,
        151, 2, 155, 1, 10, 155, 0, 99, 2, 14, 0, 0]
    intcode[1] = noun
    intcode[2] = verb
    cursor = 0
    outVal, outLoc = doOperation(intcode, cursor)
    intcode[outLoc] = outVal
    while cursor < len(intcode):
        if cursor + 4 < len(intcode):
            cursor = cursor + 4
            outVal, outLoc = doOperation(intcode, cursor)
            if outLoc != -1:
                intcode[outLoc] = outVal
            else:
                break
        else:
            break

    return intcode[0]


def doOperation(intcode, cursor):
    if intcode[cursor] == 1:
        return doOp1(intcode, cursor)
    elif intcode[cursor] == 2:
        return doOp2(intcode, cursor)
    elif intcode[cursor] == 99:
        return doOp99(intcode, cursor)
    else:
        print "Error State"
        print intcode
        sys.exit()


def doOp1(intcode, cursor):
    numberOne = intcode[intcode[cursor + 1]]
    numberTwo = intcode[intcode[cursor + 2]]
    outLoc = intcode[cursor + 3]
    outVal = numberOne + numberTwo
    return outVal, outLoc


def doOp2(intcode, cursor):
    numberOne = intcode[intcode[cursor + 1]]
    numberTwo = intcode[intcode[cursor + 2]]
    outLoc = intcode[cursor + 3]
    outVal = numberOne * numberTwo
    return outVal, outLoc


def doOp99(intcode, cursor):
    return 0, -1


if __name__ == '__main__':
    main()
