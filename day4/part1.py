#! /usr/local/bin/python2
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2019 Tiarnan de Burca <tdeburca@gmail.com>

def main():
    input = (284639,748759)
    possible_answers = range(284639, 748759)
    possible_answers = getEqualDigits(possible_answers)
    possible_answers = getIncrementingDigits(possible_answers)
    print possible_answers
    print len(possible_answers)


def getIncrementingDigits(possible_answers):
    output = set()
    for i in possible_answers:
        if incrementsOK(str(i)):
            output.add(i)
    return output


def incrementsOK(test):
    ok = True
    highnum = test[0]
    for i in test[1:]:
        if i >= highnum:
            highnum = i
        else:
            ok = False
    return ok

def getEqualDigits(possible_answers):
    output = set()
    for i in possible_answers:
        test_string = str(i)
        ok = False
        test_number = test_string[0]
        for number in test_string[1:]:
            if number == test_number:
                ok = True
            else: 
                test_number = number
        if ok == True:
            output.add(i)
    return output


            

 
if __name__ == '__main__':
    main()
