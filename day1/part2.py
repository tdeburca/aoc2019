#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2019 Tiarnan de Burca <tdeburca@gmail.com>


def main():
    infile = open("../input.txt")
    out = 0
    for i in infile:
        i = int(i)
        out = out + getModuleFuelwithFuel(i)

    print out


def getModuleFuelwithFuel(y):
    x = 0
    while getModuleFuel(y) > 0:
        x = x + getModuleFuel(y)
        y = getModuleFuel(y)
    return x


def getModuleFuel(testmass):
    return (testmass / 3) - 2


if __name__ == '__main__':
    main()
